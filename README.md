### HUA é um projeto de horta urbana automatizada que une irrigação automatizada ao reuso de materiais, incentivando e ajudando o cultivo domiciliar de alimentos. 

![HUA](https://tecnologias.libres.cc/c_arpino/hortas-urbanas-automatizadas/raw/master/hua-v1/Figuras/hortas.jpg)

[Foto do projeto no festival de tecnologias red bull basement]

## Contatos
### **HUA (Versão 1):**
- Idealizador e gestor do projeto:
    - *Rainer Grassmann* - rainer.gras@gmail.com

- Desenvolvedor:
    - *Wesley Lee* - wes@wes.am

### **HUA (Versão 2):**
- Gestores do projeto:
    - *Luiz Eduardo Davoglio Estradioto*
    - *Cristthian Marafigo Arpino* - cristthian.m.arpino@protonmail.com
 
- Desenvolvedor:
    - *Alisson Claudino de Jesus* - alissom.claudino@ufrgs.br